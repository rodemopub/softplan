import { Layout, Menu } from 'antd'
import React from 'react'
import { BrowserRouter, Redirect, Route, Switch, useHistory } from 'react-router-dom'
import { CardList } from '../features/CardList/CardList'
import { CountryDetails } from '../features/CountryDetails/CountryDetails'
import './App.css'

const { Header, Content } = Layout

export default function App() {
  const history = useHistory()

  return (
    <div className="App">
      <Layout>
        <Header>
          <Menu theme="dark" mode="horizontal">
            <Menu.Item
              onClick={() => {
                history.push('/')
              }}
            >
              HOME
            </Menu.Item>
          </Menu>
        </Header>

        <Content>
          <BrowserRouter>
            <Switch>
              <Route path="/" exact component={CardList} />
              <Route path="/:country" exact component={CountryDetails} />
              <Redirect from="*" to="/" />
            </Switch>
          </BrowserRouter>
        </Content>
      </Layout>
    </div>
  )
}
