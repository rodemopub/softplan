import { combineReducers, createStore } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import { Country, CountryMin } from '../common/types'

type State = {
  editedCountry?: Country
  list: CountryMin[]
  loaded: boolean
}

type ActionLoad = {
  type: 'LOAD'
  payload: {
    countries: CountryMin[]
  }
}

type ActionEdit = {
  type: 'EDIT'
  payload: {
    country: Country
  }
}

type ActionSubmit = {
  type: 'SUBMIT'
  payload: {
    country: Country
  }
}

type Action = ActionLoad | ActionEdit | ActionSubmit

const initialState: State = {
  editedCountry: undefined,
  list: [],
  loaded: false,
}

const modal = (state = initialState, action: Action): State => {
  switch (action.type) {
    case 'LOAD':
      return { ...state, list: action.payload.countries, loaded: true }
    case 'EDIT':
      return { ...state, editedCountry: action.payload.country }
    case 'SUBMIT':
      const { country } = action.payload
      const list = state.list.map((c) => (c.name === country.name ? country : c))
      return { ...state, list }
    default:
      return state
  }
}

const reducer = combineReducers({
  modal,
})

export const store = createStore(reducer, composeWithDevTools())

export type RootState = ReturnType<typeof store.getState>

export type AppDispatch = typeof store.dispatch
