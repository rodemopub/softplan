export const contains = (text: string, substring: string) =>
  text.toLocaleLowerCase().includes(substring.toLocaleLowerCase())

export function debounce(this: any, func: Function, timeout = 300) {
  let timer: ReturnType<typeof setTimeout>

  return (...args: any[]): void => {
    clearTimeout(timer)
    timer = setTimeout(() => {
      func.apply(this, args)
    }, timeout)
  }
}

const formatter = new Intl.NumberFormat()

export const format = (num: number) => formatter.format(num)
