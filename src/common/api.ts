const url = 'https://countries-274616.ew.r.appspot.com'

const allCountries = `query {
  Country {
    name
    nativeName
    capital
    flag {
      emoji
      svgFile
    }
  }
}`

export const getAllCountries = async () => {
  const res = await fetch(url, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ query: allCountries }),
  })

  const body = await res.json()
  return body.data.Country
}

const getQueryCountry = (name: string) => `query {
  Country(name: "${name}") {
    name
    nativeName
    area
    population
    capital
    location {
      latitude
      longitude
    }
    flag {
      emoji
      svgFile
    }
    topLevelDomains {
      name
    }
    distanceToOtherCountries(first: 5, orderBy: countryName_asc) {
      distanceInKm
      countryName
    }
  }
}`

export const getCountry = async (country: string) => {
  const res = await fetch(url, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ query: getQueryCountry(country) }),
  })

  const body = await res.json()
  const first = body.data.Country[0]
  if (first === undefined) throw new Error('País não registrado')

  return first
}
