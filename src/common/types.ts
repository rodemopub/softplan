export interface CountryMin {
  name: string
  nativeName: string
  capital: string
  flag: {
    emoji: string
    svgFile: string
  }
}

export interface Country extends CountryMin {
  area: number
  population: number
  location: {
    latitude: number
    longitude: number
  }
  topLevelDomains: [
    {
      name: string
    }
  ]
}
