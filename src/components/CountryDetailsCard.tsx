import { EditOutlined } from '@ant-design/icons'
import * as React from 'react'
import { Card, Descriptions, Space } from 'antd'
import { Country } from '../common/types'
import { format } from '../common/utils'

const { Meta } = Card
const { Item } = Descriptions

interface Props {
  country?: Country
  name: string
  loading: boolean
  onEdit: React.MouseEventHandler<HTMLSpanElement>
}

// bandeira, nome, capital, área, população e top-level domain);
// TODO: pass 'actions' as an array
export const CountryDetailsCard = ({ country, name, loading, onEdit }: Props) => (
  <Card
    style={{ width: 600 }}
    cover={<img alt={`Bandeira de ${name}`} src={country?.flag.svgFile} />}
    actions={[<EditOutlined key="edit" onClick={onEdit} />]}
    loading={loading}
  >
    {country !== undefined && (
      <Space direction="vertical" size={24}>
        <Meta title={country?.name} />
        <Descriptions>
          <Item label="Nome nativo">{country.nativeName}</Item>
          <Item label="Capital">{country.capital}</Item>
          <Item label="Área">{format(country.area)} km²</Item>
          <Item label="População">{format(country.population)}</Item>
          <Item label={`Domínio${country.topLevelDomains.length === 1 ? '' : 's'} Top-level`}>
            {country.topLevelDomains.map((t) => t.name).join(', ')}
          </Item>
        </Descriptions>
      </Space>
    )}
  </Card>
)
