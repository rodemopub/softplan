import { InfoCircleOutlined } from '@ant-design/icons';
import { Card } from 'antd';
import * as React from 'react';

const { Meta } = Card;

interface Props {
  title: string;
  description: string;
  cover: string;
  onEdit: React.MouseEventHandler<HTMLSpanElement>;
  onSeeMore: React.MouseEventHandler<HTMLSpanElement>;
}

// TODO: pass 'actions' as an array
export const CountryCard = ({ title, description, cover, onEdit, onSeeMore }: Props): JSX.Element => (
  <Card
    style={{ width: 300 }}
    cover={
      <img
        alt={`Bandeira de ${title}`}
        src={cover}
      />
    }
    actions={[
      <InfoCircleOutlined onClick={onSeeMore} />,
      // <EditOutlined  onClick={onEdit} />,
    ]}
  >
    <Meta
      title={title}
      description={description}
    />
  </Card>
)
