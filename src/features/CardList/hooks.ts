import { message } from 'antd'
import { useMemo, useRef, useState } from 'react'
import { getAllCountries } from '../../common/api'
import { CountryMin } from '../../common/types'

export const useCountries = () => {
  const [countries, setCountries] = useState<CountryMin[]>([])
  const [loading, setLoading] = useState(false)
  const abortRef = useRef<AbortController>()

  const fetchCountries = useMemo(() => async () => {
    setLoading(true)
    abortRef.current = new AbortController()

    try {
      abortRef.current?.abort()
      const res = await getAllCountries()
      setCountries(res)
    } catch (error) {
      message.error(error)
      console.error(error)
    } finally {
      setLoading(false)
    }

    return () => {
      console.error('ABORT');
      abortRef.current?.abort()
    }
  }, [])

  return { countries, setCountries, fetchCountries, loading }
}
