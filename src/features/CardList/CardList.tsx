import { List, Space } from 'antd'
import { useEffect, useMemo, useState } from 'react'
import { connect, ConnectedProps } from 'react-redux'
import { useHistory } from 'react-router'
import { AppDispatch, RootState } from '../../app/store'
import { CountryMin } from '../../common/types'
import { contains } from '../../common/utils'
import { CountryCard } from '../../components/CountryCard'
import { useCountries } from './hooks'
import { SearchCountry } from './SearchCountry'

const mapDispathToProps = (dispatch: AppDispatch) => ({
  load: (countries: CountryMin[]) => dispatch({ type: 'LOAD', payload: { countries } }),
})

const connector = connect(
  (state: RootState) => ({ list: state.modal.list, loaded: state.modal.loaded }),
  mapDispathToProps
)

type Props = ConnectedProps<typeof connector>

export const CardListComp = ({ list, loaded, load }: Props) => {
  const { countries, loading, fetchCountries } = useCountries()
  const [search, setSearch] = useState('')
  const matches = useMemo(() => list.filter((country) => contains(country.name, search)), [
    list,
    search,
  ])
  const history = useHistory()

  useEffect(() => {
    if (!loaded) {
      fetchCountries()
    }
  }, [fetchCountries, loaded])

  /* FIXME: this is a hack!
   * due to time constraints, I couldn't add and use redux-thunk
   * or other async middleware for Redux
   * this should be an thunk-action and avoid the useCountries hook
   */
  useEffect(() => {
    if (countries.length > 0 && !loaded) {
      load(countries)
    }
  }, [countries, loaded, load])

  return (
    <Space direction="vertical" style={{ width: 1000, minHeight: 'calc(100vh - 64px)' }}>
      <SearchCountry setSearch={setSearch} />

      <List
        itemLayout="vertical"
        size="large"
        grid={{ gutter: 16, column: 3 }}
        pagination={false}
        dataSource={matches}
        loading={loading}
        renderItem={(item) => (
          <List.Item key={item.name}>
            <CountryCard
              cover={item.flag.svgFile}
              title={item.name}
              key={item.name}
              description={item.capital}
              onEdit={(e: any) => {
                console.info(e) // TODO
              }}
              onSeeMore={(e: any) => {
                console.info(e)
                history.push(`/${item.name}`)
              }}
            />
          </List.Item>
        )}
      />
    </Space>
  )
}

export const CardList = connector(CardListComp)
