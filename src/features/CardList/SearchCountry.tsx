import { Input } from 'antd'
import { ChangeEvent, Dispatch, SetStateAction } from 'react'
import { debounce } from '../../common/utils'

const { Search } = Input

interface Props {
  setSearch: Dispatch<SetStateAction<string>>
}

export const SearchCountry = ({ setSearch }: Props) => {
  const onChange = (event: ChangeEvent<HTMLInputElement>) => {
    setSearch(event.target.value)
  }

  return (
    <Search
      placeholder="Busque país"
      onChange={debounce(onChange, 150)}
      style={{ maxWidth: 400, margin: 24 }}
      size="large"
    />
  )
}
