import { Form, FormInstance, Input, InputNumber } from 'antd'
import { Country } from '../../common/types'
import { format } from '../../common/utils'

const { Item } = Form

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
}
interface Props {
  country: Country
  form: FormInstance<any>
}

export const FormCountry = ({ country, form }: Props) => {
  const initialValues = {
    nome: country.name,
    nativeName: country.nativeName,
    capital: country.capital,
    svgFile: country.flag.svgFile,
    area: country.area,
    population: country.population,
  }

  return (
    <Form
      {...layout}
      form={form}
      name="basic"
      initialValues={initialValues}
      preserve={false}
    >
      <Item
        label="Nome nativo"
        name="nativeName"
        rules={[{ required: true, message: 'Por favor, escreva a nome nativo' }]}
      >
        <Input />
      </Item>

      <Item
        label="Capital"
        name="capital"
        rules={[{ required: true, message: 'Por favor, escreva a capital' }]}
      >
        <Input />
      </Item>

      <Item
        label="Bandeira"
        name="svgFile"
        rules={[{ required: true, message: 'Por favor, escreva a URL da imagem' }]}
      >
        <Input />
      </Item>

      <Item
        label="Área (km²)"
        name="area"
        rules={[{ required: true, message: 'Por favor, escreva a área' }]}
      >
        <InputNumber
          min={0}
          max={999_999_999}
          formatter={(value) => (value === undefined ? '' : format(value))}
        />
      </Item>

      <Item
        label="População"
        name="population"
        rules={[{ required: true, message: 'Por favor, escreva a população' }]}
      >
        <InputNumber
          min={0}
          max={999_999_999}
          formatter={(value) => (value === undefined ? '' : format(value))}
        />
      </Item>
    </Form>
  )
}
