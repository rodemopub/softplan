import { Form, Modal } from 'antd'
import React, { Dispatch, SetStateAction } from 'react'
import { Country } from '../../common/types'
import { FormCountry } from './FormCountry'

interface Props {
  isVisible: boolean
  setIsVisible: Dispatch<SetStateAction<boolean>>
  country?: Country
}

export const CountryModal = ({ country, isVisible, setIsVisible }: Props) => {
  const [form] = Form.useForm()

  const handleOk = () => {
    form
      .validateFields()
      .then((values) => {
        form.resetFields()
        console.info(values)
        // onCreate(values);
        setIsVisible(false)
      })
      .catch((info) => {
        console.log('Validate Failed:', info)
      })
  }

  const handleCancel = () => {
    setIsVisible(false)
  }

  return (
    <Modal
      title="Editar país"
      visible={isVisible}
      onOk={handleOk}
      onCancel={handleCancel}
      destroyOnClose
    >
      {country && <FormCountry country={country} form={form} />}
    </Modal>
  )
}
