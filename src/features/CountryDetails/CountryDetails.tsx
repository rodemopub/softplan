import { Space } from 'antd'
import { useEffect, useState } from 'react'
import { connect, ConnectedProps } from 'react-redux'
import { RouteComponentProps } from 'react-router-dom'
import { AppDispatch, RootState } from '../../app/store'
import { Country } from '../../common/types'
import { CountryDetailsCard } from '../../components/CountryDetailsCard'
import { CountryModal } from '../CountryModal/CountryModal'
import { useCountry } from './hooks'

const mapDispathToProps = (dispatch: AppDispatch) => ({
  edit: (country: Country) => dispatch({ type: 'EDIT', payload: { country } }),
})

const connector = connect(
  (state: RootState) => ({ editedCountry: state.modal.editedCountry }),
  mapDispathToProps
)

type PropsFromRedux = ConnectedProps<typeof connector>

interface RouteParams {
  country: string
}

interface Props extends PropsFromRedux, RouteComponentProps<RouteParams> {}

const CountryDetailsComp = ({
  match: {
    params: { country },
  },
  editedCountry,
  edit,
}: Props) => {
  const { country: details, loading, fetchCountry } = useCountry(country)
  const [isVisible, setIsVisible] = useState(false)

  useEffect(() => {
    fetchCountry()
  }, [fetchCountry])

  return (
    <Space direction="vertical" style={{ minHeight: 'calc(100vh - 112px)', margin: 24 }}>
      <CountryDetailsCard
        name={country}
        country={details}
        loading={loading}
        onEdit={() => {
          setIsVisible(true)
          if (details) edit(details)
        }}
      />
      <CountryModal country={editedCountry} isVisible={isVisible} setIsVisible={setIsVisible} />
    </Space>
  )
}

export const CountryDetails = connector(CountryDetailsComp)
