import { message } from 'antd'
import { useMemo, useRef, useState } from 'react'
import { getCountry } from '../../common/api'
import { Country} from '../../common/types'

export const useCountry = (name: string) => {
  const [country, setCountry] = useState<Country>()
  const [loading, setLoading] = useState(false)
  const abortRef = useRef<AbortController>()

  const fetchCountry = useMemo(() => async () => {
    setLoading(true)
    abortRef.current = new AbortController()

    try {
      abortRef.current?.abort()
      const res = await getCountry(name)
      setCountry(res)
    } catch (error) {
      message.error('Erro ao buscar dados')
      console.error(error)
    } finally {
      setLoading(false)
    }

    return () => {
      abortRef.current?.abort()
    }
  }, [name])

  return { country, setCountry, fetchCountry, loading }
}
